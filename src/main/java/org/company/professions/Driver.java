package org.company.professions;


import org.company.entities.Person;

public class Driver extends Person {
private int drivingExperience;
 public Driver (int drivingExperience){
     super("Prokopenko Alex Sergiyovich" , 31, "Male" , 0672346123);
     this.drivingExperience = drivingExperience;
 }

    public int getdrivingExperience() {
        return drivingExperience;
    }

    public void setdrivingExperience(int drivingExperience) {
        drivingExperience = drivingExperience;
    }
    public String toString(){
     return " Стаж водія : " + drivingExperience;
    }
}
