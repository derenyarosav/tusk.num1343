package org.company.entities;

public class Person {
    private String pns;
    private int age;
    private String gender;
    private int phoneNum;
    public Person(String pns, int age, String gender, int phoneNum){
        this.pns = pns;
        this.age = age;
        this.gender = gender;
        this.phoneNum = phoneNum;

    }

    public String getPns() {
        return pns;
    }

    public void setPns(String pns) {
        this.pns = pns;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }
    public String toString(){
        return " Призвіще імя по батькові : "
                + pns + " Вік : " + age + " Стать : " + gender + " Номер телефону : " + phoneNum;
    }
}
