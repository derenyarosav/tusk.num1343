package org.company.details;

public class Engine {

    private String power;
    private String manufacturer;

    public Engine(String power,String manufacturer){
    this.power = power;
    this.manufacturer = manufacturer;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String toString(){
        return " Manufactures : " + manufacturer + " Power : " + power;
    }
}

